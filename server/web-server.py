#!/usr/bin/env python 
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
from ParserHandler import get_circular_trajectory, get_rectangular_segmenter
from NeuralNetworksHandler import NeuralNetworksHandler
from NeuralNetwork import  NeuralNetwork
from random import randint
from StringIO import *
import PIL
from Image import Image
import cgi
import json
import base64
import os

class HTTPRequestHandler(BaseHTTPRequestHandler):
  
  def do_POST(self):
    context = cgi.FieldStorage( fp=self.rfile, headers=self.headers, environ={"REQUEST_METHOD": "POST"} )
    parametros = getParameters(context)

    '''https://www.youtube.com/watch?v=q5zUxN-Kvy0
    if(parametros['accion'] == "saludar"):
      print "Recibi peticion de %s" % (parametros['nombre'])
      self.send_response(200)
      #self.send_header('Content-Type', 'application/json')
      self.end_headers()
      self.wfile.write("Hola baby " + parametros['nombre'])
    '''
    self.send_response(200)
    self.send_header('Content-Type', 'application/json')
    self.end_headers()
    
    '''
    classifier = showImage(parametros['img'])
    data = {"class": classifier}
    json.dump(data, self.wfile)
    '''
    if(saveImage(parametros['img'])):
	cd = startClasification(loadNet())
	data = {"class": cd}
    	json.dump(data, self.wfile)
    
    '''
    self.wfile.write(prueba())
    '''

def getParameters(context):
  parametros = {}
  for item in context.list:
    parametros[item.name] = item.value
    #print "%s = %s" % (item.name, item.value)
  return parametros

def saveImage(str_img):
  fh = open("./img/photo.jpg", "wb")
  fh.write(str_img.decode('base64'))
  fh.close()
  return True

def showImage(str_img):
  aux = str_img.decode('base64')
  image = Image.open(StringIO(aux))
  #image.show()
  print str(image.format)
  print str(image.mode)
  print str(image.size)
  return prueba()

def prueba():
  objetos = ["perro","gato","arbol","caballo","auto","moto","poste","semaforo","desconocido"]
  return objetos[randint(0,8)]

def loadNet():
  nh = NeuralNetworksHandler()
  networkModel, netMean, prototype, classes = nh.getNetworkByIndex(0)
  nn = NeuralNetwork(networkModel,  prototype, netMean, classes)
  return nn

def startClasification(nn):
  print("Clasificando objetos en imagen")
  #numImages = self.imageProcesor.runSegmentation("img/photo.jpg")
  num = segment_entry_image("./img/photo.jpg", "./img/segments/")
  for image in range(num):
    cd = nn.classifyImage("./img/segments/cutout"+str(image)+".jpg" , image)
  return cd

def segment_entry_image(url_image, url_output):
  img = PIL.Image.open(url_image)

  width_image, height_image = img.size
  trajectory = get_circular_trajectory(width_image, height_image)
  segmenter = get_rectangular_segmenter(img, trajectory)

  i = 0
  image = segmenter.get_current_segment()
  image.pil_image.save(url_output+'cutout' + str(i) + '.jpg')

  i += 1
  while (segmenter.has_next_segment()):
    image = segmenter.get_next_segment()
    image.pil_image.save(url_output+'cutout' + str(i) + '.jpg')
    i += 1
  return i
  
  
def init():
  print('http server is starting...')
  print('============================')

  server_address = ('localhost', 8080)
  httpd = HTTPServer(server_address, HTTPRequestHandler)
  
  print('http server is running...')
  httpd.serve_forever()
  
if __name__ == '__main__':
  init()
