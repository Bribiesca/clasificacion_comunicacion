import pyaudio
import wave
import sys

class AudioResponse:
    bps = 1024

    def __init__(self, file):
        """ Iniciamos el stream """ 
        self.wf = wave.open(file, 'rb')
        self.p = pyaudio.PyAudio()
        self.stream = self.p.open(
            format = self.p.get_format_from_width(self.wf.getsampwidth()),
            channels = self.wf.getnchannels(),
            rate = self.wf.getframerate(),
            output = True
        )

    def play(self):
        """ Reproducimos el archivo de sonido """
        print("playing response")
        data = self.wf.readframes(self.bps)
        while len(data) > 0:
            self.stream.write(data)
            data = self.wf.readframes(self.bps)
        self.close()

    def close(self):
        """ Cerramos el flujo """ 
        self.stream.close()
        self.p.terminate()

#a = AudioResponse("./sounds/perro.wav")
#a.play()
#a.close()