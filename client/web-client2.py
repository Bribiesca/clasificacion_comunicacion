#!/usr/bin/env
from audio import AudioResponse
#from Preprocesor import Preprocesor
import httplib
import urllib
import json
import base64
import sys

class Client:

	def __init__(self):
		#self.url = None
		self.server = "localhost:8080"
		self.resource = "/web-server.py"
		self.headers = {"Content-type":"application/x-www-form-urlencoded","Accept":"text/plain"}
		self.util = {}

	def convertImage(self, url_image):
		imageFile = open(url_image,"rb")
		str_img = base64.b64encode(imageFile.read())
		return str_img

	def request(self, str_img):
		params = urllib.urlencode({'img': str_img})
		conn = httplib.HTTPConnection(self.server)
		conn.request("POST", self.resource, params, self.headers)
		response = conn.getresponse()
		print(response.status)
		data = response.read()
		print(data)
		conn.close()
		return self.decode(str(data))

	def decode(self, str_json):
		aux = json.loads(str_json)
		return aux['class']

	def playResponse(self, response):
		a = AudioResponse(self.util[response])
		a.play()
		a.close()

	def run(self):
		#self.takePicture()
		self.playResponse(self.request(self.convertImage("./img/perritu.jpg")))

	def load(self):
		filee = open("./util.txt","r")
		for line in filee:
			aux_line = line.rstrip("\n") 
			aux = aux_line.split(" ")
			self.util[aux[0]] = aux[1]
		#print(self.util)
		filee.close()
	
	############################################################################################################################	

c = Client()
c.load()
c.run()
#c = Client()
#c.startClasification()

